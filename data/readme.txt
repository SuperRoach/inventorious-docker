These are used to have a persistent mount of your database between stuff happening like stopping containers.
By having a sqlitedb.db file in here, It will be copied across.
If you are reading this from your application's /data folder, congratulations! the volume is working correctly.

Sidenote: If you use docker-compose with an empty folder, 
it will recreate a database using the mock_data.json file. It's also a great way to get an initial dataset populated!